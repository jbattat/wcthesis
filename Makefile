all:
	pdflatex main
	bibtex main
	pdflatex main
	pdflatex main

chap1 chap2: %: ch-%
	open $<.pdf

# Does not work with bibliography...
# https://stackoverflow.com/questions/637227/makefile-for-multi-file-latex-document
ch-%: %.tex
	pdflatex --shell-escape --jobname=ch-$* \
	  "\includeonly{$*}\input{main}"

clean:
	rm -f *.aux *.log *~ *.lof *.toc *.out *.bbl *.blg *.info *.lot

tar:
	tar zvcf wcthesis.tar.gz ../thesis

cleanall: 
	rm main.pdf
