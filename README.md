# Wellesley College Physics Thesis Template

This LaTeX thesis template is tailored to Wellesley College's requirements.  It is a fork of an MIT undergraduate thesis template.

To build the entire thesis, run  
`$ make`

which will make a PDF file called `main.pdf` containing your entire thesis.

To build a single chapter, run  
`$ make CHAPTERNAME`  
where `CHAPTERNAME.tex` is the LaTeX file containing the chapter that you want to build.  This produces a PDF file called `ch-CHAPTERNAME.pdf` containing just the chapter you requested. It also opens the PDF file for viewing.

At present, the chapter numbering for single-chapter builds will always be 1 (i.e. not necessarily accurate), and the citations are not functional (`bibtex` is not called).